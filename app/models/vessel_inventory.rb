class VesselInventory < ActiveRecord::Base
	attr_accessible :mdm_tugboat_id, :location, :parts_name, :interval_period, :interval_period_unit, 
		:interval_period_changeable, :position, :last_change_record_period, :last_change_record_date, 
		:last_change_record_executor, :current_hours, :next_change_date, :next_change_hours

	belongs_to :mdm_tugboat

	def self.special_save(value, boat_id, position)
    vi = VesselInventory.new(value)
    vi.mdm_tugboat_id = boat_id
    vi.position = position
    vi.interval_period_changeable = false if vi.interval_period_unit == 'Months'
    vi.save
	end

	def params_calculation(par)
    if par[:last_change_record_date]
      if par[:interval_period_unit] == 'Hours'
        par[:next_change_hours] = self.interval_period + par[:last_change_record_period].to_i - par[:current_hours].to_i
        par[:next_change_date] = par[:last_change_record_date].to_date + (par[:next_change_hours].to_i / 24).days
      else
        par[:next_change_date] = par[:last_change_record_date].to_date + self.interval_period.months
      end
    end
    nil
	end

end
