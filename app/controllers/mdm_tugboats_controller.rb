class MdmTugboatsController < ApplicationController
  # GET /mdm_tugboats
  # GET /mdm_tugboats.json
  def index
    @mdm_tugboats = MdmTugboat.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mdm_tugboats }
    end
  end

  # GET /mdm_tugboats/1
  # GET /mdm_tugboats/1.json
  def show
    @mdm_tugboat = MdmTugboat.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mdm_tugboat }
    end
  end

  # GET /mdm_tugboats/new
  # GET /mdm_tugboats/new.json
  def new
    @mdm_tugboat = MdmTugboat.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mdm_tugboat }
    end
  end

  # GET /mdm_tugboats/1/edit
  def edit
    @mdm_tugboat = MdmTugboat.find(params[:id])
  end

  # POST /mdm_tugboats
  # POST /mdm_tugboats.json
  def create
    @mdm_tugboat = MdmTugboat.new(params[:mdm_tugboat])

    respond_to do |format|
      if @mdm_tugboat.save
        format.html { redirect_to @mdm_tugboat, notice: 'Mdm tugboat was successfully created.' }
        format.json { render json: @mdm_tugboat, status: :created, location: @mdm_tugboat }
      else
        format.html { render action: "new" }
        format.json { render json: @mdm_tugboat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mdm_tugboats/1
  # PUT /mdm_tugboats/1.json
  def update
    @mdm_tugboat = MdmTugboat.find(params[:id])

    respond_to do |format|
      if @mdm_tugboat.update_attributes(params[:mdm_tugboat])
        format.html { redirect_to @mdm_tugboat, notice: 'Mdm tugboat was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mdm_tugboat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mdm_tugboats/1
  # DELETE /mdm_tugboats/1.json
  def destroy
    @mdm_tugboat = MdmTugboat.find(params[:id])
    @mdm_tugboat.destroy

    respond_to do |format|
      format.html { redirect_to mdm_tugboats_url }
      format.json { head :no_content }
    end
  end
end
