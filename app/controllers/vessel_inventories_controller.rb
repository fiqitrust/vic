class VesselInventoriesController < ApplicationController
  # GET /vessel_inventories
  # GET /vessel_inventories.json
  def index
    @vessel_inventories = {}
    # VesselInventory.order('created_at').all(:include => :mdm_tugboat)
    # tug_ids = @vessel_inventories.map{|x| x.mdm_tugboat_id}.uniq
    @tugboats = MdmTugboat.order('name').includes(:vessel_inventories).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @vessel_inventories }
    end
  end

  # GET /vessel_inventories/1
  # GET /vessel_inventories/1.json
  def show
    @vessel_inventory = VesselInventory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @vessel_inventory }
    end
  end

  # GET /vessel_inventories/new
  # GET /vessel_inventories/new.json
  def new
    @vessel_inventory = VesselInventory.new
    @tugboats = MdmTugboat.order('name').all
    @values = {}
    @values['MAIN ENGINE'] = ['FUEL INJECTION PUMP', 'INJECTOR NOZZLE', 'FO FILTER', 'LO FILTER', 'LUB OIL', 'BATTERY']
    @values['GENERATOR'] = ['INJECTOR NOZZLE', 'FO FILTER', 'LO FILTER', 'BATTERY']
    @values['DECK'] = ['RADAR', 'GPS', 'BATTERY', 'LIFE JACKET', 'LIFE BUOY', 'SEARCH LIGHT', 'MOORING ROPE', 'TOWING SHACKLESS', 'TOWING WIRE', 'TOWING ROPE']

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @vessel_inventory }
    end
  end

  # GET /vessel_inventories/1/edit
  def edit
    @vessel_inventory = VesselInventory.find(params[:id], :include => :mdm_tugboat)
  end

  # POST /vessel_inventories
  # POST /vessel_inventories.json
  def create
    params[:vic].each do |key, value|
      if params[:amount][key] == '1'
        VesselInventory.special_save(value, params[:boat_id], 'PORT')
        VesselInventory.special_save(value, params[:boat_id], 'STBD')
      else
        if params[:amount][key].blank?
          VesselInventory.special_save(value, params[:boat_id], nil)
        else
          params[:amount][key].to_i.times {|num| VesselInventory.special_save(value, params[:boat_id], 'PORT #'+(num+1).to_s)}
          params[:amount][key].to_i.times {|num| VesselInventory.special_save(value, params[:boat_id], 'STBD #'+(num+1).to_s)}
        end
      end
    end

    redirect_to :action => 'index', notice: 'Vessel inventory was successfully created.'
    # @vessel_inventory = VesselInventory.new(params[:vessel_inventory])

    # respond_to do |format|
    #   if @vessel_inventory.save
    #     format.html { redirect_to @vessel_inventory, notice: 'Vessel inventory was successfully created.' }
    #     format.json { render json: @vessel_inventory, status: :created, location: @vessel_inventory }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @vessel_inventory.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /vessel_inventories/1
  # PUT /vessel_inventories/1.json
  def update
    @vessel_inventory = VesselInventory.find(params[:id])
    @vessel_inventory.params_calculation(params[:vessel_inventory])
# binding.pry
    respond_to do |format|
      if @vessel_inventory.update_attributes(params[:vessel_inventory])
        format.html { redirect_to vessel_inventories_url, notice: 'Vessel inventory was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @vessel_inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vessel_inventories/1
  # DELETE /vessel_inventories/1.json
  def destroy
    @vessel_inventory = VesselInventory.find(params[:id])
    @vessel_inventory.destroy

    respond_to do |format|
      format.html { redirect_to vessel_inventories_url }
      format.json { head :no_content }
    end
  end
end
