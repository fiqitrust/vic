module ApplicationHelper
  def active_toggle(menu)
    if menu == 'master'
      'active' if ['mdm_tugboats'].include?(params[:controller])
    else
      'active' if menu == params[:controller]
    end
  end
end