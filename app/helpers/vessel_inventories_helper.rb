module VesselInventoriesHelper
  def hours_months_print(str1, str2)
    '<option value="'+str1+'">'+str1+'</option><option value="'+str2+'">'+str2+'</option>'
  end

  def hours_or_months(iteration, location)
    data = location == 'DECK' ? hours_months_print('Months', 'Hours').html_safe : hours_months_print('Hours', 'Months').html_safe
    select_tag 'vic['+iteration.to_s+'][interval_period_unit]', data, :class => 'text_field'
  end

  def row_style(ncd)
    if ncd
      range = (ncd - Time.now.to_date).to_i
      if range < 0
        'class=vic-danger'
      else
        if range < 40
          'class=vic-alert'
        else
          ''
        end
      end
    else
      ''
    end
  end
end
