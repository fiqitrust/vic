require 'test_helper'

class VesselInventoriesControllerTest < ActionController::TestCase
  setup do
    @vessel_inventory = vessel_inventories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vessel_inventories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vessel_inventory" do
    assert_difference('VesselInventory.count') do
      post :create, vessel_inventory: @vessel_inventory.attributes
    end

    assert_redirected_to vessel_inventory_path(assigns(:vessel_inventory))
  end

  test "should show vessel_inventory" do
    get :show, id: @vessel_inventory
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vessel_inventory
    assert_response :success
  end

  test "should update vessel_inventory" do
    put :update, id: @vessel_inventory, vessel_inventory: @vessel_inventory.attributes
    assert_redirected_to vessel_inventory_path(assigns(:vessel_inventory))
  end

  test "should destroy vessel_inventory" do
    assert_difference('VesselInventory.count', -1) do
      delete :destroy, id: @vessel_inventory
    end

    assert_redirected_to vessel_inventories_path
  end
end
