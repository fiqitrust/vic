require 'test_helper'

class MdmTugboatsControllerTest < ActionController::TestCase
  setup do
    @mdm_tugboat = mdm_tugboats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mdm_tugboats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mdm_tugboat" do
    assert_difference('MdmTugboat.count') do
      post :create, mdm_tugboat: @mdm_tugboat.attributes
    end

    assert_redirected_to mdm_tugboat_path(assigns(:mdm_tugboat))
  end

  test "should show mdm_tugboat" do
    get :show, id: @mdm_tugboat
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mdm_tugboat
    assert_response :success
  end

  test "should update mdm_tugboat" do
    put :update, id: @mdm_tugboat, mdm_tugboat: @mdm_tugboat.attributes
    assert_redirected_to mdm_tugboat_path(assigns(:mdm_tugboat))
  end

  test "should destroy mdm_tugboat" do
    assert_difference('MdmTugboat.count', -1) do
      delete :destroy, id: @mdm_tugboat
    end

    assert_redirected_to mdm_tugboats_path
  end
end
