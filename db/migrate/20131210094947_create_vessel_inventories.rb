class CreateVesselInventories < ActiveRecord::Migration
  def change
    create_table :vessel_inventories do |t|
      t.integer     :mdm_tugboat_id
      t.string      :location
      t.string      :parts_name
      t.integer     :interval_period
      t.string      :interval_period_unit
      t.boolean     :interval_period_changeable, :default => true
      t.string      :position
      t.integer     :last_change_record_period, :default => 0
      t.date        :last_change_record_date
      t.string      :last_change_record_executor
      t.integer     :current_hours, :default => 0
      t.integer     :next_change_hours, :default => 0
      t.date        :next_change_date
      t.timestamps
    end
  end
end
