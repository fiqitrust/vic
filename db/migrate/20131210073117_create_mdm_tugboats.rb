class CreateMdmTugboats < ActiveRecord::Migration
  def change
    create_table :mdm_tugboats do |t|
      t.string :code
      t.string :name
      t.integer :std_consm_empty
      t.integer :std_consm_loaded
      t.integer :std_consm_idle
      t.integer :uom_id
      t.integer :company_id
      t.integer :std_speed_empty
      t.integer :std_speed_loaded
      t.integer :speed_uom_id
      t.text :description
      t.string :picture_file_name
      t.string :picture_content_type
      t.integer :picture_file_size
      t.datetime :picture_updated_at  

      t.timestamps
    end
  end
end
