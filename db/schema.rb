# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131210094947) do

  create_table "mdm_tugboats", :force => true do |t|
    t.string   "code"
    t.string   "name"
    t.integer  "std_consm_empty"
    t.integer  "std_consm_loaded"
    t.integer  "std_consm_idle"
    t.integer  "uom_id"
    t.integer  "company_id"
    t.integer  "std_speed_empty"
    t.integer  "std_speed_loaded"
    t.integer  "speed_uom_id"
    t.text     "description"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "vessel_inventories", :force => true do |t|
    t.integer  "mdm_tugboat_id"
    t.string   "location"
    t.string   "parts_name"
    t.integer  "interval_period"
    t.string   "interval_period_unit"
    t.boolean  "interval_period_changeable",  :default => true
    t.string   "position"
    t.integer  "last_change_record_period",   :default => 0
    t.date     "last_change_record_date"
    t.string   "last_change_record_executor"
    t.integer  "current_hours",               :default => 0
    t.integer  "next_change_hours",           :default => 0
    t.date     "next_change_date"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

end
